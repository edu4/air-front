myApp.controller('PortfolioListCtrl',
  ['$scope', '$http',
    function ($scope, $http) {

      var driveFolderUrl = $("#all-samples").attr('href');
      var linksContainer = $("#sample-list");
      $scope.documents = [];

      getDocuments()

      // Topic: Statically hosted (S3) website for an airbnb 3rdparty service to create nice copywriting for offers.
      // Implement a way to show all the samples in the list (in linksContainer).
      // The samples are in a public Google drive folder, referenced by driveFolderUrl.
      // Think about user experience and data fetching
      function getDocuments() {

        $http({
          method: 'GET',
          url: 'http://localhost:3000/api/v1/documents/'
        }).then(function successCallback(response) {
          console.log(response);
          $scope.documents = response.data;
          // this callback will be called asynchronously
          // when the response is available
        }, function errorCallback(response) {
          console.log(response);
          // called asynchronously if an error occurs
          // or server returns response with an error status.
        });
      }

      // console.log(driveFolderUrl);
      // console.log(linksContainer);

    }]); // Controller